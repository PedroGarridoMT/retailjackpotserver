import * as logResponse from '../json/commonSettings/logResponse.json';
import * as monetaryResponse from '../json/commonSettings/monetaryResponse.json';
import * as viewerResponse from '../json/commonSettings/viewerResponse.json';
import * as websocketResponse from '../json/commonSettings/websocketResponse.json';

class CommonSettingsController {
  public getLogConfig() {
    return logResponse;
  }
  public getMonetaryConfig() {
    return monetaryResponse;
  }
  public getViewerConfig() {
    return viewerResponse;
  }
  public getWebsocketConfig() {
    return websocketResponse;
  }
}
export default CommonSettingsController;

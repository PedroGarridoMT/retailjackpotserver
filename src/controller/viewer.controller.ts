import * as centerJSON from '../json/viewer/centerResponse.json'
import * as deviceJSON from '../json/viewer/deviceResponse.json'
import {DevicesService} from '../devices/devices.service';
import {Device} from './models';
import {DeviceInfo} from '../devices/models';

// remove default property from import
const {default:_c, ...center} = centerJSON as any;
const {default:_d, ...device} = deviceJSON as any;

class ViewerController {

  public async getDeviceInfo(req:any): Promise<Device> {
		const deviceUid = (req && req.query && req.query.deviceUid) as string;
    return this.getDevice(deviceUid);
  }

  private async getDevice(deviceUid?: string): Promise<Device> {
		const info = await (deviceUid ?
			this.findDeviceInfo(deviceUid) :
			DevicesService.instance.info
		);

		return {
			...device,
			deviceUid: info.playerUid,
			centerCode: info.centerName
		};
  }

	async findDeviceInfo(deviceUid: string): Promise<DeviceInfo> {
		const devices = await DevicesService.instance.devices;
		return devices.find(d => d.playerUid = deviceUid);
	}

  public getCenterInfo() {
		return this.getCenter();
  }

  public getCenter() {
		return center;
  }
}

export default ViewerController;

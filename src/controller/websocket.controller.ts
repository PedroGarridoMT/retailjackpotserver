import * as websocketLocalResponse from '../json/websocket/websocketLocalResponse.json';
class WebsocketController {
  public getWebsocketLocal() {
    return websocketLocalResponse;
  }
}
export default WebsocketController;

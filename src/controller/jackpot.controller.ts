import * as gsConfig from '../json/jackpot/config.json';
import ViewerController from './viewer.controller';
import {FileLogger} from '../utils/logger';
import * as https from 'https';
import * as http from 'http';
import {DeviceInfo} from 'devices/models';
const METHOD_LOGIN = '/api/usermanagement/login';

class JackpotController {
	private endPoint: string;
	private casinoUid: string;
	private size: number;
	private viewerController: ViewerController;
	private _loginRequest: Promise<any>;

	constructor() {
		this.endPoint = gsConfig.gsEndpoint;
		this.casinoUid = gsConfig.casinoUid;
		this.size = gsConfig.prizeSize;
		this.viewerController = new ViewerController();
	}

	// ---- Jackpot Status ---
	public async getJackpotStatus(req, res, retry) {
		await this.requestAndRetry(req, res, retry, this.requestJackpotStatus);
	}

	// ---- Jackpot Prizes ---
	public async getJackpotPrizes(req, res, retry) {
		await this.requestAndRetry(req, res, retry, this.requestJackpotPrizes);
	}

	// Function Retries
	private async requestAndRetry(req, res, retry, requestToGs){
		let cookie = await this.getGsHeaders();
		if (!cookie) {
			res.status(401);
			res.send({});
		}
		requestToGs(cookie, this)
			.then((response)=> {
				res.send(response);
				res.status(response ? 200 : 500);
			}).catch((e) => {
			console.error(e);
			if(e.statusCode === 401 && retry){
				this._loginRequest = null;
				this.requestAndRetry(req, res, false, requestToGs);
			} else {
				res.send({message: e.message});
				res.status(e.statusCode);
			}
		})
	}

	private requestJackpotStatus(headers, _this): Promise<any> {
		const requestUid = Math.floor(Math.random() * 110);
		const url = `/api/jackpots/casinos/${_this.casinoUid}/jackpot-status?requestUid=${requestUid}`;
		return _this.requestJackpot(headers, url, _this.jackpotStatusMapping, _this);
	}

	private requestJackpotPrizes(headers, _this): Promise<any> {
		const requestUid = Math.floor(Math.random() * 110);
		const url = `/api/jackpots/casinos/${_this.casinoUid}/prizes?requestUid=${requestUid}&size=${_this.size}`;
		return _this.requestJackpot(headers, url, _this.jackpotPrizesMapping, _this);
	}

	private requestJackpot(headers, url, jackpotMapping, _this): Promise<any> {
		const optionsPOST = {
			hostname: _this.endPoint,
			path: url,
			method: 'GET',
			protocol: 'https:',
			headers: headers
		};
		let data = '';
		return new Promise((resolve, reject) => {
			https.get(optionsPOST, responsePOST => {
				if(_this.checkResponseReturnCode(responsePOST)) {
					responsePOST.on('data', d => data += d);
					responsePOST.on('end', () => {
						const d = jackpotMapping(data, _this);
						resolve(d);
					})
					responsePOST.on('error', err => {
						_this.logRequestError(err.message, url);
						reject({statusCode: 500, message: err.message});
					});
				} else {
					reject({statusCode: responsePOST.statusCode, message: responsePOST.statusMessage});
				}
			});
		});
	}

	// ---- Helpers Login ---
	private getGsCredentials() {
		return this.decodeBase64(this.decodeBase64(gsConfig.gsCredentials));
	}

	private decodeBase64(encode) {
		return Buffer.from(encode, 'base64').toString();
	}

	private getOptionsLogin(body: any) {
		return {
			hostname: this.endPoint,
			path: METHOD_LOGIN,
			method: 'POST',
			protocol: 'https:',
			headers: {
				'Content-Type': 'application/json',
				'Content-Length': body.length
			}
		};
	}

	// ---- Helpers GS header ---
	private async getGsHeaders(): Promise<any> {
		if( !this._loginRequest){
			const bodyLogin = this.getGsCredentials();
			const optionLogin = this.getOptionsLogin(bodyLogin);
			this._loginRequest = this.loginRequest(optionLogin, bodyLogin);
		}
		return await this._loginRequest;
	}

	private loginRequest(optionLogin, bodyLogin): Promise<any> {
		return new Promise((resolve, reject) => {
			const loginRequest = https.request(optionLogin, response => {
				this.checkResponseReturnCode(response);
				console.log('Login request done - date: ', new Date());
				resolve(this.makeGsHeaders(response));
			});
			loginRequest.on('error', error => {
				this.logRequestError(error.message, optionLogin.path)
				reject(error);
			})
			loginRequest.write(bodyLogin);
			loginRequest.end();
		});
	}

	private makeGsHeaders(response) {
		const auth = this.authHeader(response);
		const token = this.tokenHeader(response);
		const cookie = `authorization=${auth}; XSRF-TOKEN=${token}`;
		return {
			'Content-Type': 'application/json',
			'X-XSRF-TOKEN': token,
			'Cookie': cookie
		};
	}

	private authHeader(response) {
		let auth_rx = /authorization=(.*)$/g;
		return this.getHeaderValue(response, auth_rx, 0);
	}

	private tokenHeader(response) {
		let token_rx = /XSRF-TOKEN=(.*)$/g;
		return this.getHeaderValue(response, token_rx, 1);
	}

	private getHeaderValue(response, pattern, position) {
		let auth_arr = pattern.exec(response.headers['set-cookie'][position]);
		return auth_arr[1].slice(0, auth_arr[1].indexOf(';'));
	}

	// ---- Helpers Jackpot Status ---
	private jackpotStatusMapping(data, _this) {
		let dataJson: any;
		try {
			dataJson = JSON.parse(data);
		} catch (e) {
			return null;
		}
		let levels = [];
		dataJson.levelStatuses.forEach(level => {
			levels.push({
				levelName: level.name,
				maxAmount: level.maxAmount,
				minBet: level.minLastBet,
				amount: level.amount
			});
		});
		return {
			jackpotName: dataJson.name,
			currency: dataJson.currency,
			levels: levels
		};
	}

	// ---- Helpers Jackpot Prizes ---
	private async jackpotPrizesMapping(data: string, _this) {
		let dataJson: Array<any>;
		try {
			dataJson = JSON.parse(data);
		} catch (e) {
			return null;
		}
		const prizes = [];
		for (const item of dataJson) {
			prizes.push({
				levelName: item.levelName,
				amount: item.amount,
				currency: item.currency,
				timestamp: item.timestamp,
				center: await _this.getCenterInfo(item.playerUid)
			});
		}
		return prizes;
	}

	private async getCenterInfo(playerUid: string) {
		let center = this.viewerController.getCenter();
		const device = (await this.viewerController.findDeviceInfo(playerUid)) || {} as DeviceInfo;

		return {
			city: device.city || center.city,
			name: device.centerName || center.name,
			address: center.address,
			centerUid: center.centerUid
		};
	}

	private logRequestError(message: string, url: string) {
		FileLogger.instance.error(`Error on request ${url} from GS`, message );
	}

	private checkResponseReturnCode(message: http.IncomingMessage): boolean {
		if (message.statusCode < 200 || message.statusCode > 299) {
			this.logRequestError(`status code: ${message.statusCode}`, message.url);
			return false;
		}
		return true;
	}

}
export default JackpotController;

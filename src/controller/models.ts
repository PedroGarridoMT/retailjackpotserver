export interface Device {
  centerUid: string;
  centerCode: string;
  externalCenterCode: string;
  deviceUid: string;
  deviceCode: string;
  deviceType: string;
}

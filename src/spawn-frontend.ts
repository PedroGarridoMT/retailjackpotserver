import * as fs from 'fs';
import { spawn, ChildProcess } from 'child_process';


export function spawnFrontEndProcess(clientPath: string, args?: string[]): ChildProcess {
	if (!fs.existsSync(clientPath)) {
		throw new Error(`FrontEnd not found at ${clientPath}`);
	}
	console.log(`Lauching frontend app: ${clientPath}`);
	const client = spawn(clientPath, args || []);
	client.stdout.on('data', data => {
		process.stdout.write(`FrontEnd: ${data.toString()}`);
		// logger.instance.debug(`FrontEnd: ${data.toString()}`);
	});
	client.stderr.on('data', data => {
		process.stderr.write(`FrontEnd: ${data.toString()}`);
		// logger.instance.error(`FrontEnd: ${data.toString()}`);
	});
	return client;
}

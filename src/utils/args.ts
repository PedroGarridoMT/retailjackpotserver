
export namespace Args {
	const args: string[] = process.argv.slice(1);

	function findIndex(name: string): number {
		const argName = name.toLowerCase();
		const matches = [
			argName,
			`-${argName}`,
			`--${argName}`
		];

		return args
			.map(a => a.toLowerCase())
			.findIndex(a => matches.some(m => m === a));
	}

	export function getArgValue(name: string): string {
		const index = findIndex(name);
		return (index === -1 || index >= args.length - 1) ?
			null :
			args[index + 1];
	}

	export function hasArg(name: string): boolean {
		return findIndex(name) >= 0;
	}
}


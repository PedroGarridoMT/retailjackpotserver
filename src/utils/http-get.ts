import * as http from 'http';

export namespace HttpRequest {
	export async function get<T>(url: string): Promise<T> {
		return new Promise((resolve, reject) => {
			http.get(url, (res) => {
				const {statusCode} = res;
				const contentType = res.headers['content-type'];

				let error: string;
				if (statusCode !== 200) {
					error = 'Request Failed.\n' +
						`Status Code: ${statusCode}`;
				} else if (!/^application\/json/.test(contentType)) {
					error = 'Invalid content-type.\n' +
						`Expected application/json but received ${contentType}`;
				}
				if (error) {
					reject(error);
					res.resume();
					return;
				}

				res.setEncoding('utf8');
				let rawData = '';
				res.on('data', (chunk) => {rawData += chunk;});
				res.on('end', () => {
					try {
						const parsedData = JSON.parse(rawData);
						resolve(parsedData);
					} catch (e) {
						reject(e.message);
					}
				});
			}).on('error', (e) => {
				reject(e.message);
			});
		});
	}
}



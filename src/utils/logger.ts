import * as log4js from 'log4js';
import * as config from '../json/serverSettings/log.json';

const { level: defaultlevel, default:_, ...settings} = config as any;

export class FileLogger {
	private static _instance: log4js.Logger;
	private static _level: string;
	private static shutedDown = false;

	static init(level?: string) {
		FileLogger._level = level || defaultlevel;
		FileLogger._instance = FileLogger.createInstance(level);
	}

	static get initialized(): boolean {
		return Boolean(this._instance);
	}

	static get instance(): log4js.Logger {
		if (!FileLogger._instance) {
			throw new Error('Logger no initialized');
		}
		return FileLogger._instance;
	}

	static get level(): string {
		return this._level;
	}

	private static createInstance(level?: string): log4js.Logger {
		log4js.configure({
			appenders: {
				local: {
					type: 'file',
					...settings
				}
			},
			categories: {
				default: {appenders: ['local'], level: level || defaultlevel}
			}
		});

		return log4js.getLogger();
	}

	public static shutdown(callback?: (e:Error) => void) {
		if (!FileLogger.shutedDown) {
			FileLogger.shutedDown = true;
			log4js.shutdown(callback);
		}
	}
}


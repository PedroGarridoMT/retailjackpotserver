import {FileLogger} from './utils/logger';
import { startServer } from './server';
import { spawnFrontEndProcess } from './spawn-frontend';
import { Args } from './utils/args';
import * as serverSettings from './json/serverSettings/server.json';
import { ChildProcess } from 'child_process';

let frontEnd: ChildProcess;

process.on('uncaughtException', err => {
	console.error(err);
	if (FileLogger.initialized) {
		FileLogger.instance.fatal('Main process unhandled exception: ', err);
		FileLogger.shutdown(() => process.exit(1));
	} else {
		process.exit(1);
	}
});

process.on('unhandledRejection', err => {
	console.error(err);
	if (FileLogger.initialized) {
		FileLogger.instance.error('Main process unhandled rejection: ', err);
	}
});

process.on('exit', code => {
	console.log(`Main process exited with code ${code}`)
	if (FileLogger.initialized) {
		FileLogger.shutdown();
	}
	if (Boolean(frontEnd)) {
		frontEnd.kill();
	}
});


const logLevel = Args.getArgValue('log-level');
const isDebug = Args.hasArg('front-debug');
const serverPort = serverSettings.port;
const executableExtension = process.platform === 'win32' ? '.exe' : '';
const frontFullPath = serverSettings.frontEndPath + executableExtension;
const frontArgs = isDebug ? ['debug'] : [];

FileLogger.init(logLevel);

startServer(serverPort).then(server => {
	server.on('close', () => process.exit(1));
	try {
		frontEnd = spawnFrontEndProcess(frontFullPath, frontArgs);
	} catch(e) {
		console.error('ERROR: ', e);
		FileLogger.instance.fatal('Frontend lauching error: ', e);
		throw e;
	}
	frontEnd.on('close', code => {
		console.log(`Frontend process exited with code ${code}`)
		process.exit(code);
	});
});

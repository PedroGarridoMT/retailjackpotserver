import { HttpRequest } from '../utils/http-get';
import {DeviceInfo} from './models';

export class DevicesService {
	private readonly API_ENDPOINT = 'http://localhost:60061';
	private static _instance: DevicesService;
	private _info: Promise<DeviceInfo>;
	private _devices: Promise<Array<DeviceInfo>>;


	public static get instance(): DevicesService {
		if (!DevicesService._instance) {
			DevicesService._instance = new DevicesService();
		}
		return DevicesService._instance;
	}


	get info(): Promise<DeviceInfo> {
		if (!this._info) {
			const resource = this.getResourceUri('info');
			this._info = HttpRequest.get<DeviceInfo>(resource);
				//.catch(e => this.handleError(resource, e)) as Promise<DeviceInfo>;
		}
		return this._info;
	}

	get devices(): Promise<Array<DeviceInfo>> {
		if (!this._devices) {
			const resource = this.getResourceUri('devices');
			this._devices = HttpRequest.get<Array<DeviceInfo>>(resource);
				// .catch(e => this.handleError(resource, e)) as Promise<Array<DeviceInfo>>;
		}
		return this._devices;
	}


	private getResourceUri(resource: string): string {
		return `${this.API_ENDPOINT}/${resource}`;
	}

	private handleError(url: string, e: Error) {
		throw new Error(`Request ${url} FAILED: ${e.message} `);
		// console.error(`Request ${url} FAILED: `, e);
		// process.exit(1);
	}

}

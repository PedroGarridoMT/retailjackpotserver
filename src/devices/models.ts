export interface DeviceInfo {
	playerUid: string;
	centerName: string;
	city: string;
}

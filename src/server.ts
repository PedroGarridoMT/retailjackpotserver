import https from 'https';
import fs from 'fs';
import path from 'path';
import express from 'express';
import os from 'os';
import * as bodyParser from 'body-parser';
import CommonSettingsController from './controller/commonSettings.controller';
import JackpotController from './controller/jackpot.controller';
import WebsocketController from './controller/websocket.controller';
import ViewerController from './controller/viewer.controller';
import {FileLogger} from './utils/logger';
import {dump} from 'js-yaml';


export async function startServer(port: number): Promise<https.Server> {
	const app = express();

	app.use(bodyParser.json());

	app.use(function (_req, res, next) {
		res.header("Access-Control-Allow-Origin", "*");
		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		next();
	});

	//---------- CommonSettings ----------
	let commonSettingsController = new CommonSettingsController();
	app.get('/api/CommonSettings/log', (_req, res) =>
		res.send({
			...commonSettingsController.getLogConfig(),
			logLevel: FileLogger.level.toUpperCase()
		})
	);
	app.get('/api/CommonSettings/Monetary', (_req, res) =>
		res.send(commonSettingsController.getMonetaryConfig())
	);
	app.get('/api/CommonSettings/viewer', (_req, res) =>
		res.send(commonSettingsController.getViewerConfig())
	);
	app.get('/api/CommonSettings/websocket', (_req, res) =>
		res.send(commonSettingsController.getWebsocketConfig())
	);

	//---------- Viewer ----------
	let viewerController = new ViewerController();
	app.get('/api/viewer/center', (_req, res) =>
		res.send(viewerController.getCenterInfo())
	);
	app.get('/api/viewer/device', (req, res) => {
			viewerController.getDeviceInfo(req).then(d => {
				res.send(d);
			});
		}
	);

	//---------- Jackpot ----------
	let jackpotController = new JackpotController();
	app.get('/api/jackpot/status', (_req, res) =>
		jackpotController.getJackpotStatus(_req, res, true)
	);
	app.get('/api/jackpot/prizes', (_req, res) =>
		jackpotController.getJackpotPrizes(_req, res, true)
	);

	//---------- Websocket ----------
	let websocketController = new WebsocketController();
	app.get('/api/Websocket/Local', (_req, res) =>
		res.send(websocketController.getWebsocketLocal())
	);


	app.post('/log*', (req, res) => {
		const instance = FileLogger.instance;
		const logEntryLevel = (req.body.level as string).toLowerCase();
		
		let parsed: string;
		try {
			parsed = dump(req.body, {skipInvalid: true});
		} catch (_) {
			parsed = JSON.stringify(req.body, null, 2);
		}

		if (instance[logEntryLevel]) {
			instance[logEntryLevel](' FRONT: ' + os.EOL + parsed);
		} else {
			instance.debug(os.EOL + parsed);
		}

		res.send({});
	});

	app.get('/', (_req, res) =>
		res.send('Viewer API server')
	);

	const certificatesPath = path.join(__dirname, '..', 'certificates')

	return new Promise((resolve, _reject) => {
		const server = https.createServer({
			key: fs.readFileSync(path.join(certificatesPath, 'cert.key')),
			cert: fs.readFileSync(path.join(certificatesPath, 'cert.crt')),
		}, app);

		server.listen(port,  () => {
			resolve(server);
			console.log(`server is listening on ${port}`);
		});
	});
}

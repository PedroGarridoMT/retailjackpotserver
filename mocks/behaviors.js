try {
  const { Behavior } = require('@mocks-server/main');

  const devices = require('./fixtures/devices.json');

  const standard = new Behavior(
		devices
  );


  exports = module.exports = {
    standard,
  };
} catch(e) {
  console.error(e);
  throw(e);
}

